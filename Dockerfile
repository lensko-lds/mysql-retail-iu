FROM php:7.4.28-cli
RUN docker-php-ext-install pdo_mysql
WORKDIR /usr/src/script
COPY PASSWORD PORT product.csv store.csv main.php ./
CMD [ "php", "./main.php" ]
