#!/bin/bash

# Declare constants
readonly PROJECT=$(<PROJECT)
readonly SERVER=$PROJECT-server

# Connect to MySQL container
docker exec -it "$SERVER" mysql --default-character-set=utf8 -u root -p$(<PASSWORD) test
