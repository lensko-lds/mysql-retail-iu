#!/bin/bash

# Generate password
if [ ! -f PASSWORD ]; then
    (echo -n Pw1_; md5sum /var/lib/dbus/machine-id | cut -f 1 -d " ") > PASSWORD
fi

# Declare constants
readonly PASSWORD=$(<PASSWORD)
readonly PORT=$(<PORT)
readonly PROJECT=$(<PROJECT)
readonly CLIENT=$PROJECT-client
readonly SERVER=$PROJECT-server
readonly SQL="
UPDATE mysql.user SET Host='%' WHERE User='root';
FLUSH PRIVILEGES;
CREATE DATABASE test;
"

# Stop and remove containers
./stop.sh

# Start MySQL container
docker run -d --rm --name="$SERVER" -p "$PORT":3306 -e MYSQL_ROOT_PASSWORD="$PASSWORD" mysql/mysql-server:8.0.28
while [ "`docker inspect -f {{.State.Health.Status}} $SERVER`" != "healthy" ]; do sleep 1; done
docker exec -i "$SERVER" mysql -u root -p${PASSWORD} <<< "$SQL"

# Build PHP image and Start PHP container
docker build -t "$CLIENT" .
docker rmi $(docker images -f dangling=true -q)
docker run -dt --rm --name "$CLIENT" --add-host host.docker.internal:host-gateway "$CLIENT"

# List containers
./status.sh
