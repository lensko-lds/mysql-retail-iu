#!/bin/bash

# Declare constants
readonly PROJECT=$(<PROJECT)

# List containers
docker ps --format "table {{.Names}}\t {{.Status}}\t {{.Ports}}" --filter "name=$PROJECT-"
